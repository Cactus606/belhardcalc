﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BelHardClass02Calculator
{

    class Program
    {
        static void Main(string[] args)
        {
            string input = "";

            string pattern = @"([\^|/|*|+|-])";
            double firstArgument = 1;
            double secondArgument = 1;
            double thirdArgument = 1;

            double equation = 0;

            string firstOperator;
            string secondOperator;
            Regex regex = new Regex(pattern);

            while (input != "Exit")
            {
                input = Console.ReadLine();

                if (input == "Exit")
                    break;

                //parsed = new string[5];

                string[] parsed = regex.Split(input.ToString());

                if (parsed.Length > 5)
                    Console.WriteLine("Переполнение");
                else {

                    firstArgument = Convert.ToDouble(parsed[0]);
                    secondArgument = Convert.ToDouble(parsed[2]);
                    thirdArgument = Convert.ToDouble(parsed[4]);

                    firstOperator = parsed[1];
                    secondOperator = parsed[3];
                    Calc calc = new Calc();

                    try
                    {

                        if (parsed[3] == "^")
                        {
                            equation = calc.DoMath(secondArgument, thirdArgument, secondOperator);
                            equation = calc.DoMath(firstArgument, equation, firstOperator);
                            Console.WriteLine(equation);
                        }
                        else if (parsed[1] != "^" && parsed[3] == "*" || parsed[3] == "/")
                        {
                            equation = calc.DoMath(secondArgument, thirdArgument, secondOperator);
                            equation = calc.DoMath(firstArgument, equation, firstOperator);
                            Console.WriteLine(equation);
                        }
                        else {
                            equation = calc.DoMath(firstArgument, secondArgument, firstOperator);
                            equation = calc.DoMath(equation, thirdArgument, secondOperator);
                            Console.WriteLine(equation);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Something went wrong", e);
                    }
                }
            }
        }
    }
}

