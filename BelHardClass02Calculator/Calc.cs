﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BelHardClass02Calculator
{
    class Calc
    {
        public double DoMath(double firstArgument, double secondArgument, string operation)
        {
            double temp = 1;
            switch (operation)
            {
                case "^":
                    temp = Math.Pow(firstArgument, secondArgument);
                    break;
                case "*":
                    temp = firstArgument * secondArgument;
                    break;
                case "/":
                    temp = firstArgument / secondArgument;
                    break;
                case "-":
                    temp = firstArgument - secondArgument;
                    break;
                case "+":
                    temp = firstArgument + secondArgument;
                    break;
            }
            return temp;
        }
    }
}
